package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by josepita on 13/08/2017.
 */
public class LandingPage extends BasePage {
    @FindBy(name = "register")
    private WebElement registerButton;

    @FindBy(name = "birthDateYear")
    private WebElement birthDateYearSelect;

    @FindBy(name = "birthDateMonth")
    private WebElement birthDateMonthSelect;

    @FindBy(name = "birthDateDay")
    private WebElement birthDateDaySelect;

    @FindBy(name = "province")
    private WebElement provinceSelect;

    @FindBy(name = "email")
    private WebElement emailTxtBx;

    @FindBy(name = "userLogin")
    private WebElement userLoginTxtBx;

    @FindBy(name = "password")
    private WebElement passwordTxtBx;

    @FindBy(css = "a[href='http://gmail.com']")
    private WebElement goToGmailButton;


    public LandingPage(WebDriver driver) {
        super(driver);
        isLoaded();
    }

    @Override
    public boolean isLoaded() {
        waitForElementToBeVisibleWithTimeOut(registerButton,20);
        return registerButton.isDisplayed();
    }

    private void selectBirthDateDay(String Day){
        Select select = new Select(birthDateDaySelect);
        select.selectByVisibleText(Day);
    }

    private void selectBirthDateMonth(String Month){
        Select select = new Select(birthDateMonthSelect);
        select.selectByVisibleText(Month);
    }

    private void selectBirthDateYear(String Year){
        Select select = new Select(birthDateYearSelect);
        select.selectByVisibleText(Year);
    }

    private void selectProvince(String Province){
        Select select = new Select(provinceSelect);
        select.selectByVisibleText(Province);
    }

    private void fillEmail(String Email)
    {
        emailTxtBx.clear();
        emailTxtBx.sendKeys(Email);
    }

    private void fillUserLogin(String UserLogin)
    {
        System.out.println(UserLogin);
        userLoginTxtBx.clear();
        userLoginTxtBx.sendKeys(UserLogin);
    }

    private void fillPassword(String Password){
        passwordTxtBx.clear();
        passwordTxtBx.sendKeys(Password);
    }

    private void clickRegisterButton(){
        registerButton.click();
    }

    public void fillRegisterFormWithValidData(){
        long timestamp = System.currentTimeMillis() / 1000L;
        selectBirthDateDay("01");
        selectBirthDateMonth("enero");
        selectBirthDateYear("1985");
        fillEmail("pitatest+"+timestamp+"@gmail.com");
        fillUserLogin("pitatest"+timestamp);
        fillPassword("testing");
    }

    public void fillAndConfirmRegisterFormWithValidData(){
        fillRegisterFormWithValidData();
        clickRegisterButton();
    }

    public Boolean wasRegisterProcessASuccess(){
        fillAndConfirmRegisterFormWithValidData();
        return waitForElementToNotBeVisibleWithTimeOut(By.name("register"), 10)&&
                goToGmailButton.isDisplayed();
    }


    public boolean areWarningsShowingFillingEmptyForm() {
        clickRegisterButton();
        return driver.findElements(By.className("tooltipOwn")).size() == 4;
    }

    public boolean areWarningsShowingFillingFormWithInvalidData() {
        clickRegisterButton();
        List<WebElement> warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("La fecha introducida es incorrecta"))
            return false;
        if(!warnings.get(1).getText().equals("El email no puede estar vacío"))
            return false;
        if(!warnings.get(2).getText().equals("El usuario no puede estar vacío"))
            return false;
        if(!warnings.get(3).getText().equals("La contraseña no puede estar vacía"))
            return false;

        selectBirthDateDay("01");
        selectBirthDateMonth("enero");
        selectBirthDateYear("1985");

        fillEmail("asd"+ Keys.TAB);

        warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("El email introducido es incorrecto"))
            return false;

        fillEmail("asd@asasd.com"+ Keys.TAB);
        warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("El email introducido es incorrecto"))
            return false;

        fillEmail("asd@gmail.com"+ Keys.TAB);
        warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("El usuario no puede estar vacío"))
            return false;

        fillUserLogin("asd"+ Keys.TAB);
        warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("El usuario introducido es demasiado corto"))
            return false;

        fillUserLogin("asdasd"+ Keys.TAB);
        warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("La contraseña no puede estar vacía"))
            return false;

        fillPassword("12345"+ Keys.TAB);
        warnings = driver.findElements(By.className("tooltipOwn"));
        if(!warnings.get(0).getText().equals("La contraseña introducida es demasiado corta"))
            return false;

        fillPassword("123456"+ Keys.TAB);
        warnings = driver.findElements(By.className("tooltipOwn"));

        return warnings.size()==0;
    }
}
