package tests;

import org.testng.annotations.Test;
import pages.LandingPage;

import static org.testng.Assert.assertTrue;

public class RegisterUserTest extends BaseTest{

    @Test(description = "test that we can register a user")
    public void registerValidUserTest(){
        LandingPage landingPage = new LandingPage(driver);
        assertTrue(landingPage.isLoaded(), "landing page not loaded");
        assertTrue(landingPage.wasRegisterProcessASuccess(), "user was not created properly");
    }

    @Test(description = "test that the form is validated when is submited with all fields empty")
    public void checkWarningsFillingEmptyFormTest(){
        LandingPage landingPage = new LandingPage(driver);
        assertTrue(landingPage.isLoaded(), "landing page not loaded");
        assertTrue(landingPage.areWarningsShowingFillingEmptyForm(), "Warnings not showing");
    }

    @Test(description = "test that the form is validated on each field")
    public void checkWarningsFillingFormInvalidDataTest(){
        LandingPage landingPage = new LandingPage(driver);
        assertTrue(landingPage.isLoaded(), "landing page not loaded");
        assertTrue(landingPage.areWarningsShowingFillingFormWithInvalidData(), "Warnings not showing");
    }
}