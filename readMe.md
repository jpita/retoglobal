# TECHNICAL TEST

### HOW TO RUN THE TESTS 
In order to run the tests, Google Chrome and Gradle need to be installed. 

Then, either import the project into an IDE and run the tests using testng or `cd` onto the folder and run:

`gradle test`

The report for the results of the tests can be seen at 

`/build/reports/tests/packages/tests.html`


### REQUESTED TESTS

1. Se ha de automatizar el flujo de login y registro de un nuevo usuario en
https://www.goduo.com

2. Se ha de verificar que cuando los datos son incorrectos aparecen
los errores que tocan
3. Se ha de verificar que cuando los datos son correctos el usuario se crea y se puede loguear.
4. Extra - Montar un docker-compose con todos los requisitos de la prueba.


### COMPLETED TESTS
1. `@Test(description = "test that we can register a user")`
##### `registerValidUserTest()`

2. `@Test(description = "test that the form is validated on each field")`
##### `checkWarningsFillingFormInvalidDataTest()`

3. `@Test(description = "test that the form is validated when is submited with all fields empty")`
##### `checkWarningsFillingEmptyFormTest()`

### NOTES

* I wasn't able to do the login part because I would need an email to get the email confirmation link or a link generator api endpoint to validate the email and then login.

* I wasn't able to do the docker part